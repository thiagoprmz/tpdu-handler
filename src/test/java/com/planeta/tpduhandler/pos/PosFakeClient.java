package com.planeta.tpduhandler.pos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class PosFakeClient {
	private Socket clientSocket;
	private DataOutputStream out;
	private DataInputStream in;

	// Inicia conexao com balancer/tpdu handler 
	public void startConnection(String ip, int port) throws UnknownHostException, IOException {
		clientSocket = new Socket(ip, port);
		out = new DataOutputStream(clientSocket.getOutputStream());
		in = new DataInputStream(clientSocket.getInputStream());
	}

	// Envia bytes e retorna buffer recebido
	public byte[] sendMessage(byte[] bytes) {
		byte[] buffer = new byte[8192];
		try {
			out.write(bytes);
			in.read(buffer);
		} catch (IOException e) {
			e.printStackTrace();
			return buffer;
		}
		return buffer;
	}

	// Fecha conexao
	public void stopConnection() {
		try {
			in.close();
			out.close();
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
