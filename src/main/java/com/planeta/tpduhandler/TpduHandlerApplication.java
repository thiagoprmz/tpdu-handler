package com.planeta.tpduhandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpduHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpduHandlerApplication.class, args);
	}

}
