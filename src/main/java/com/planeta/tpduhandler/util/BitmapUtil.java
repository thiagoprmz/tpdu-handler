package com.planeta.tpduhandler.util;

import java.util.HashMap;
import java.util.Map;

import com.planeta.tpduhandler.model.iso8385.CustomField48;
import com.planeta.tpduhandler.model.iso8385.CustomField60;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoValue;


public class BitmapUtil {

	public static Map<Object, Object> loadDataElements(IsoMessage m) {
		Map<Object, Object> dataElements = new HashMap<>(128);
		for (int i = 0; i < 128; i++) {
			if (m.getField(i) != null) {
				translateField(i, m.getField(i), dataElements);
				//System.out.println(i + " " + m.getField(i));
			}
		}
		//System.out.println(dataElements);	
		return dataElements;
	}

	public static Map<Object, Object> loadDataElementsIntKey(IsoMessage m) {
		Map<Object, Object> dataElements = new HashMap<>(128);
		for (int i = 0; i < 128; i++) {
			if (m.getField(i) != null) {
				translateFieldInt(i, m.getField(i), dataElements);
			}
		}
		return dataElements;
	}

	public static Map<Object, Object> loadDataElements(String bitmapBits, byte[] bytes, int startData) {
		Map<Object, Object> dataElements = new HashMap<>(128);
		int arrayPosition = startData;
		for (int i = 0; i < bitmapBits.length(); i++) {
			arrayPosition = parse(i + 1, bitmapBits.charAt(i), dataElements, bytes, arrayPosition);
		}
		return dataElements;
	}

	private static void translateField(int pos, IsoValue<?> value, Map<Object, Object> dataElements) {
		switch (pos) {
		case 1:
			dataElements.put("hasSecondaryBitMap", value.toString());
			break;
		case 2:
			dataElements.put("PrimaryAccountNumber", value.toString());
			break;
		case 3:
			dataElements.put("ProcessingCode", value.toString());
			break;
		case 4:
			dataElements.put("TransactionAmount", value.toString());
			break;
		case 7:
			dataElements.put("TransmissionDateAndTime", value.toString());
			break;
		case 11:
			dataElements.put("SystemsTraceAuditNumber", value.toString());
			break;
		case 12:
			dataElements.put("LocalTransactionTime", value.toString());
			break;
		case 13:
			dataElements.put("LocalTransactionDate", value.toString());
			break;
		case 14:
			dataElements.put("ExpirationDate", value.toString());
			break;
		case 22:
			dataElements.put("PointOfServeDataCode", value.toString());
			break;
		case 23:
			dataElements.put("CardSequenceNumber", value.toString());
			break;
		case 35:
			dataElements.put("Track2Data", value.toString());
			break;
		case 38:
			dataElements.put("AuthorizationIdentificationResponse", value.toString());
			break;
		case 39:
			dataElements.put("ResponseCode", value.toString());
			break;
		case 41:
			dataElements.put("CardAcceptorTerminalIdentification", value.toString());
			break;
		case 42:
			dataElements.put("CardAcceptorIdentificationCode", value.toString());
			break;
		case 45:
			dataElements.put("Track1Data", value.toString());
			break;
		case 48:
			//dataElements.put("ExtendedTerminalData", ((CustomField48) value.getEncoder()).getFields());
			break;
		case 49:
			dataElements.put("TransactionCurrencyCode", value.toString());
			break;
		case 52:
			dataElements.put("PINData", value.toString());
			break;
		case 54:
			dataElements.put("AdditionalAmounts", value.toString());
			break;
		case 55:
			dataElements.put("IntegratedCircuitCardSystemRelatedData", value.toString());
			break;
		case 57:
			dataElements.put("SecurityRelatedControlInformation", value.toString());
			break;
		case 58:
			dataElements.put("LastApprovedSRRN", value.toString());
			break;
			//		case 60:
			//			dataElements.put("ExtendedPaymentData", ((CustomField60) value.getEncoder()).getFields());
			//			break;
		default:
			dataElements.put(Integer.toString(pos), value.toString());
			break;
		}

	}

	private static void translateFieldInt(int pos, IsoValue<?> value, Map<Object, Object> dataElements) {
		switch (pos) {
		case 48:
			dataElements.put(pos, ((CustomField48) value.getEncoder()).getFields());
			break;
		case 60:
			dataElements.put(pos, ((CustomField60) value.getEncoder()).getFields());
			break;
		default:
			dataElements.put(pos, value.toString());
			break;
		}

	}

	private static int parse(int pos, char bit, Map<Object, Object> dataElements, byte[] bytes, int arrayPosition) {
		// se o bit for zero, nao tem o que fazer
		if (bit == '0')
			return arrayPosition;

		int numBytes;
		String tempValue;
		switch (pos) {
		case 1:
			dataElements.put("hasSecondaryBitMap", true);
			arrayPosition++;
			break;
		case 2:
			// TODO: como processar LLVAR?
			numBytes = Integer.parseInt(String.format("%x", bytes[arrayPosition]));
			arrayPosition++;
			tempValue = extractData(bytes, arrayPosition, numBytes);
			arrayPosition += numBytes;
			dataElements.put("PrimaryAccountNumber", tempValue);
			break;
		case 3:
			numBytes = 6;
			tempValue = extractData(bytes, arrayPosition, numBytes);
			arrayPosition += numBytes;
			dataElements.put("ProcessingCode", tempValue);
			break;
		case 4:
			numBytes = 8; // alterei pra 8 pra dar certo, mas tem algo errado
			tempValue = extractData(bytes, arrayPosition, numBytes);
			arrayPosition += numBytes;
			dataElements.put("TransactionAmount", tempValue);
			break;
		case 7:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "TransmissionDateAndTime", 10);
			break;
		case 11:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "SystemsTraceAuditNumber", 6);
			break;
		case 12:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "LocalTransactionTime", 6);
			break;
		case 13:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "LocalTransactionDate", 4);
			break;
		case 14:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "ExpirationDate", 4);
			break;
		case 22:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "PointOfServeDataCode", 3);
			break;
		case 23:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "CardSequenceNumber", 3);
			break;
		case 35:
			// TODO: como processar LLVAR?
			//System.out.println(arrayPosition);
			numBytes = Integer.parseInt(String.format("%x", bytes[arrayPosition++]));
			//System.out.println("NB: " + numBytes);
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "Track2Data", numBytes);
			break;
		case 38:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "AuthorizationIdentificationResponse", 6);
			break;
		case 39:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "ResponseCode", 2);
			break;
		case 41:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "CardAcceptorTerminalIdentification", 8);
			break;
		case 42:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "CardAcceptorIdentificationCode", 15);
			break;
		case 45:
			// TODO: como processar LLVAR?
			numBytes = Integer.parseInt(String.format("%x", bytes[arrayPosition]));
			//System.out.println("NB: " + numBytes);
			arrayPosition++;
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "Track1Data", numBytes);
			break;
		case 48:
			// TODO: como processar LLVAR?
			numBytes = Integer.parseInt(String.format("%x%x", bytes[arrayPosition++],bytes[arrayPosition++]));
			//System.out.println("NB: " + numBytes);
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "ExtendedTerminalData", numBytes);
			break;
		case 49:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "TransactionCurrencyCode", 3);
			break;
		case 52:
			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "PINData", 8);
			break;
			// Tem um problema no parse desse LLLVAR
			//		case 54:
			//			// TODO: como processar LLVAR?
			//			numBytes = Integer.parseInt(String.format("%x%x", bytes[arrayPosition++],bytes[arrayPosition++]));
			//			System.out.println("NB: " + numBytes);
			//			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "AdditionalAmounts", numBytes);
			//			break;
			//		case 55:
			//			// TODO: como processar LLVAR?
			//			numBytes = Integer.parseInt(String.format("%x%x", bytes[arrayPosition++],bytes[arrayPosition++]));
			//			System.out.println("NB: " + numBytes);
			//			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "IntegratedCircuitCardSystemRelatedData", numBytes);
			//			break;
			//		case 57:
			//			// TODO: como processar LLVAR?
			//			numBytes = Integer.parseInt(String.format("%x%x", bytes[arrayPosition++],bytes[arrayPosition++]));
			//			System.out.println("NB: " + numBytes);
			//			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "SecurityRelatedControlInformation", numBytes);
			//			break;
			//		case 58:
			//			// TODO: como processar LLVAR?
			//			numBytes = Integer.parseInt(String.format("%x%x", bytes[arrayPosition++],bytes[arrayPosition++]));
			//			System.out.println("NB: " + numBytes);
			//			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "LastApprovedSRRN", numBytes);
			//			break;
			//		case 60:
			//			// TODO: como processar LLLVAR?
			//			numBytes = Integer.parseInt(String.format("%x%x", bytes[arrayPosition++],bytes[arrayPosition++]));
			//			System.out.println("NB: " + numBytes);
			//			numBytes = 80;
			//			arrayPosition = processBitPosition(dataElements, bytes, arrayPosition, "ExtendedPaymentData", numBytes);
			//			break;
		default:
			break;
		}

		return arrayPosition;
	}

	/**
	 * @param dataElements
	 * @param bytes
	 * @param arrayPosition
	 * @return
	 */
	private static int processBitPosition(Map<Object, Object> dataElements, byte[] bytes, int arrayPosition, String dataElementName, int numBytes) {
		//System.out.print(dataElementName + " -> ");
		String tempValue = extractData(bytes, arrayPosition, numBytes);
		arrayPosition += numBytes;
		dataElements.put(dataElementName, tempValue);
		return arrayPosition;
	}

	/**
	 * @param bytes
	 * @param arrayPosition
	 * @param numBytes
	 * @return
	 */
	private static String extractData(byte[] bytes, int arrayPosition, int numBytes) {
		String tempValue = "";
		//System.out.println(String.format("pos: %d, nb: %d, %s", arrayPosition, numBytes, ByteUtils.bytesToHex(Arrays.copyOfRange(bytes, arrayPosition, arrayPosition+numBytes))));
		byte[] tmp = new byte[numBytes];
		int count = 0;
		for (int i = arrayPosition; i < arrayPosition+numBytes; i++) {
			tempValue += (char) bytes[i];
			tmp[count]=bytes[i];
			count++;
		}
		//System.out.println(String.format("pos: %d, nb: %d, %s", arrayPosition, numBytes, ByteUtils.bytesToHex(tmp)));

		return tempValue;
	}
}
