package com.planeta.tpduhandler.util;

import java.nio.ByteBuffer;

public class ByteUtils {
    private static ByteBuffer longBuffer = ByteBuffer.allocate(Long.BYTES);    
    private static ByteBuffer intBuffer = ByteBuffer.allocate(Integer.BYTES);    
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
	
    public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
    
    public static byte[] longToBytes(long x) {
    	longBuffer.putLong(0, x);
        return longBuffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
    	longBuffer.put(bytes, 0, bytes.length);
    	longBuffer.flip();//need flip 
        return longBuffer.getLong();
    }
    
    public static byte[] intToBytes(int x) {
    	intBuffer.putInt(0, x);
        return intBuffer.array();
    }

    public static int bytesToInt(byte[] bytes) {
    	intBuffer.put(bytes, 0, bytes.length);
    	intBuffer.flip();//need flip 
        return intBuffer.getInt();
    }
}
