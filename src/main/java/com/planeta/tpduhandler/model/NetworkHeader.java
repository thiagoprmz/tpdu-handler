package com.planeta.tpduhandler.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.planeta.tpduhandler.util.ByteUtils;


public class NetworkHeader implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	private byte[] bytes;
	private Long length;
	private String headerId;

	public NetworkHeader() {
	}

	public NetworkHeader(byte[] bytes) {
		super();
		this.bytes = bytes;
		parse();
	}

	public void parse() {
		length = Long.parseLong(String.format("%c%c%c%c", bytes[0], bytes[1], bytes[2], bytes[3]));
		headerId = "";
		for (int i = 0; i < length; i++) {
			headerId += String.format("%x", bytes[4+i]);
		}
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public Long getLength() {
		return length;
	}

	public void setLength(Long length) {
		this.length = length;
	}

	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NetworkHeader [length=");
		builder.append(length);
		builder.append(", headerId=");
		builder.append(headerId);
		builder.append(", bytes=");
		builder.append(ByteUtils.bytesToHex(bytes));
		builder.append("]");
		return builder.toString();
	}

}
