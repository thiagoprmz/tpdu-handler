package com.planeta.tpduhandler.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.planeta.tpduhandler.util.ByteUtils;


public class Message implements Serializable {
	private static Logger logger = LoggerFactory.getLogger(Message.class);

	private static final long serialVersionUID = 1L;

	private byte[] bytes;
	@JsonIgnore
	private byte[] protocolHeader;
	private Tpdu tpdu;
	private NetworkHeader networkHeader;
	private ApplicationMessage applicationMessage;
	@JsonIgnore
	private Long crc;
	@JsonIgnore
	private boolean parseSuccess;

	public Message() {
	}

	public Message(byte[] bytes) {
		this.bytes = bytes;
		if (bytes.length > 12) {
			parseSuccess = parse();
		}
	}

	public boolean parse() {

		try {
			//long t1 = System.currentTimeMillis();
			protocolHeader = new byte[] {bytes[0], bytes[1]};
			tpdu = new Tpdu(Arrays.copyOfRange(bytes, 2, 7));
			int networkHeaderLength = Integer.parseInt(String.format("%c%c%c%c", bytes[7], bytes[8], bytes[9], bytes[10]));
			int applicationMessageStart = 7 + 4 + networkHeaderLength;
			networkHeader = new NetworkHeader(Arrays.copyOfRange(bytes, 7 , applicationMessageStart));
			//long t2 = System.currentTimeMillis();

			applicationMessage = new ApplicationMessage(Arrays.copyOfRange(bytes, applicationMessageStart , bytes.length));
			return applicationMessage.isParseSuccess();
		} catch (IOException e) {
			logger.error("Erro ao fazer o parse da mensagem ErrorMessage: {}  byesTpdu:  ", e, ByteUtils.bytesToHex(bytes) );
			return false;
		}
	}

	@JsonIgnore
	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public int getByteCount() {
		if(this.bytes != null) {
			return this.bytes.length;
		}
		return 0;
	}

	public byte[] getProtocolHeader() {
		return protocolHeader;
	}

	public Tpdu getTpdu() {
		return tpdu;
	}

	public NetworkHeader getNetworkHeader() {
		return networkHeader;
	}

	public ApplicationMessage getApplicationMessage() {
		return applicationMessage;
	}

	public Long getCrc() {
		return crc;
	}

	public boolean isParseSuccess() {
		return parseSuccess;
	}

	/*	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		try {
			builder.append("\"protocolHeader\":");
			builder.append(mapper.writeValueAsString(this.getProtocolHeader()));
			builder.append(",\"tpdu\":");
			builder.append(mapper.writeValueAsString(this.getTpdu()));
			builder.append(",\"messageType\":");
			builder.append(mapper.writeValueAsString(this.getApplicationMessage().getMessageType()));
			builder.append(",\"dataElements\":");
			builder.append(mapper.writeValueAsString(this.getApplicationMessage().getDataElementsAsString()));

		} catch (JsonProcessingException e) {
			logger.error(e.getMessage(), e);
		}
		builder.append("}");
		return builder.toString();
	}*/

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Message [protocolHeader=");
		builder.append(ByteUtils.bytesToHex(protocolHeader));
		builder.append(", tpdu=");
		builder.append(tpdu);
		builder.append(", networkHeader=");
		builder.append(networkHeader);
		builder.append(", applicationMessage=");
		builder.append(applicationMessage);
		builder.append(", crc=");
		builder.append(crc);
		builder.append(", bytes=");
		builder.append(ByteUtils.bytesToHex(bytes));
		builder.append("]");
		return builder.toString();
	}

}
