package com.planeta.tpduhandler.model.iso8385;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.CustomField;

public class CustomField60 implements CustomField<CustomField60> {
	private static Logger logger = LoggerFactory.getLogger(CustomField60.class);

	private String value;
	private Map<String, String> fields;

	/**
	 * 60: 0602144Celular,OP:VIVO,USER:vivo,PASS:vivo,GAPN:gprsnac.com.br,GPIN:8486,COND:RMDM,MDMSERIAL:354870051403366,SIMCARDID:89551080437000937006,GSIGNAL:40%0603061MODEL:MP20,APP:1.03.09,EMVVR:,PAG:0214h,BTDKD:false,BTCRG:80%0604045TMS:1/0,B24:0/0,PRE:0/0,REC:0/0,LOY:0/0,DES:0
	 * Bit 60 quebrado
	 * 
	 * 0602144 - Celular,OP:VIVO,USER:vivo,PASS:vivo,GAPN:gprsnac.com.br,GPIN:8486,COND:RMDM,MDMSERIAL:354870051403366,SIMCARDID:89551080437000937006,GSIGNAL:40%
	 * 0603061 - MODEL:MP20,APP:1.03.09,EMVVR:,PAG:0214h,BTDKD:false,BTCRG:80%
	 * 0604045 - TMS:1/0,B24:0/0,PRE:0/0,REC:0/0,LOY:0/0,DES:0
	 */
	public static void main(String[] args) {
		String d = "0602144Celular,OP:VIVO,USER:vivo,PASS:vivo,GAPN:gprsnac.com.br,GPIN:8486,COND:RMDM,MDMSERIAL:354870051403366,SIMCARDID:89551080437000937006,GSIGNAL:40%0603061MODEL:MP20,APP:1.03.09,EMVVR:,PAG:0214h,BTDKD:false,BTCRG:80%0604045TMS:1/0,B24:0/0,PRE:0/0,REC:0/0,LOY:0/0,DES:0";
		System.out.println(new CustomField60().decodeField(d));
	}

	public CustomField60() {
		fields = new HashMap<>();
	}

	@Override
	public CustomField60 decodeField(String value) {
		logger.debug("Decoding {}: {}", this.getClass().getName(), value);
		this.value = value;
		fields.clear();
		String s = value;
		String key, fieldData;
		int numChars;
		while (s.length() > 8) {
			key = s.substring(0, 7);
			numChars = Integer.parseInt(key.substring(4, key.length()));
			logger.debug("Decoding {}: key={}, numChars={}", this.getClass().getName(), key, numChars);
			fieldData = s.substring(7, 7 + numChars);
			logger.debug("Decoding {}: key={}, data={}", this.getClass().getName(), key, fieldData);
			fields.put(key, fieldData);
			if (s.length() > 0)
				s = s.substring(7 + numChars);
			logger.debug("Decoding {}: info to parse={}", this.getClass().getName(), s);
		}

		return this;
	}

	@Override
	public String encodeField(CustomField60 customField48) {
		return customField48.value;
	}

	public String getValue() {
		return value;
	}

	public Map<String, String> getFields() {
		return fields;
	}

	@Override
	public String toString() {
		return fields.toString();
	}

}
