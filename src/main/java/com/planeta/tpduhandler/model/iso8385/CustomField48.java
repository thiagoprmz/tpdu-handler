package com.planeta.tpduhandler.model.iso8385;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.CustomField;

public class CustomField48 implements CustomField<CustomField48> {
	private static Logger logger = LoggerFactory.getLogger(CustomField48.class);

	private String value;
	private Map<String, String> fields;

	/**
	 * Bit 48 quebrado
	 * 
	 * 0101005 - 0214h 0102004 - MP20 0103020 - 8100011704000746 0104007 - 1.03.09
	 * 0105107 - 1.03.09 0214hMP20
	 * 20181017111936000541000541002002004000000000100002000030000000000000000080000
	 * 0405003 - PMO 0502045 - GERTEC BRASIL LTDA 01800 0503003 - 009
	 */
	public static void main(String[] args) {
		String d = "01010050214h0102004MP2001030208100011704000746    01040071.03.0901051071.03.09   0214hMP20           201810171119360005410005410020020040000000001000020000300000000000000000800000405003PMO0502045GERTEC BRASIL LTDA                      018000503003009";
		d = "0108001101010050214e0102015MOVE25003GEMW-C010302016933301051705302162010400810.09.21";
		System.out.println(new CustomField48().decodeField(d));
	}

	public CustomField48() {
		fields = new HashMap<>();

	}

	@Override
	public CustomField48 decodeField(String value) {
		logger.debug("Decoding {}: {}", this.getClass().getName(), value);
		this.value = value;
		fields.clear();
		String s = value;
		String key, fieldData;
		int numChars;
		while (s.length() > 8) {
			key = s.substring(0, 7);
			numChars = Integer.parseInt(key.substring(4, key.length()));
			logger.debug("Decoding {}: key={}, numChars={}", this.getClass().getName(), key, numChars);
			fieldData = s.substring(7, 7 + numChars);
			logger.debug("Decoding {}: key={}, data={}", this.getClass().getName(), key, fieldData);
			fields.put(key, fieldData);
			if (s.length() > 0)
				s = s.substring(7 + numChars);
			logger.debug("Decoding {}: info to parse={}", this.getClass().getName(), s);
		}

		return this;
	}

	@Override
	public String encodeField(CustomField48 customField48) {
		return customField48.value;
	}

	public String getValue() {
		return value;
	}

	public Map<String, String> getFields() {
		return fields;
	}

	@Override
	public String toString() {
		return fields.toString();
	}

}
