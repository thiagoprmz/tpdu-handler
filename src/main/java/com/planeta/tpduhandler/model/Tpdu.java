package com.planeta.tpduhandler.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.planeta.tpduhandler.util.ByteUtils;


public class Tpdu implements Serializable {
	private static final long serialVersionUID = 1L;

	private byte[] bytes;
	private byte id;
	private Integer origin;
	private Integer destination;

	public Tpdu() {
	}

	public Tpdu(byte[] bytes) {
		super();
		this.bytes = bytes;
		parse();
	}
	
	private void parse() {
		this.id = bytes[0];
		this.origin = Integer.valueOf((bytes[1] << 8) + bytes[2]);
		this.destination = Integer.valueOf((bytes[3] << 8) + bytes[4]);
	}

	@JsonIgnore
	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
		parse();
	}

	public byte getId() {
		return id;
	}

	public void setId(byte id) {
		this.id = id;
	}

	public Integer getOrigin() {
		return origin;
	}
	
	public String getOriginAsHexString() {
		return String.format("0x%04x", origin);
	}

	public void setOrigin(Integer origin) {
		this.origin = origin;
	}

	public Integer getDestination() {
		return destination;
	}
	
	public String getDestinationAsHexString() {
		return String.format("0x%04x", destination);
	}

	public void setDestination(Integer destination) {
		this.destination = destination;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Tpdu [bytes=");
		builder.append(ByteUtils.bytesToHex(bytes));
		builder.append(", id=");
		builder.append(String.format("0x%02x", id));
		builder.append(", origin=");
		builder.append(String.format("0x%04x", origin));
		builder.append(", destination=");
		builder.append(String.format("0x%04x", destination));
		builder.append("]");
		return builder.toString();
	}

}
