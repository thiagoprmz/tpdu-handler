package com.planeta.tpduhandler.model;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.planeta.tpduhandler.util.BitmapUtil;
import com.planeta.tpduhandler.util.ByteUtils;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.MessageFactory;




public class ApplicationMessage implements Serializable {
	private static Logger logger = LoggerFactory.getLogger(ApplicationMessage.class);

	private static final long serialVersionUID = 1L;

	@JsonIgnore
	private byte[] bytes;
	@JsonIgnore
	private byte[] bitmap;
	private String messageType;
	private Map<Object, Object> dataElements;
	private MessageFactory<IsoMessage> mf;
	private IsoMessage isoMessage;
	private boolean parseSuccess;

	public ApplicationMessage() throws IOException {
		init();
	}

	public ApplicationMessage(byte[] bytes) throws IOException {
		super();
		this.bytes = bytes;
		init();
		parseSuccess = parse();
	}

	private void init() throws IOException {
		mf = new MessageFactory<>();
		mf.setCharacterEncoding("ISO-8859-1");
		//mf.setCustomField(48, new CustomField48());
		//mf.setCustomField(60, new CustomField60());
		mf.setConfigPath("config.xml");
	}

	public boolean parse() {
		try {
			isoMessage = mf.parseMessage(bytes, 0);
			messageType = String.format("%c%c%c%c", bytes[0], bytes[1], bytes[2], bytes[3]);
			bitmap = Arrays.copyOfRange(bytes, 4, 12);
			dataElements = BitmapUtil.loadDataElements(isoMessage);
			return true;
		} catch (UnsupportedEncodingException | ParseException e) {
			logger.info("Failure parsing message: ErrorMessage: {}  Error: {}  byesTpdu: {}", e.getMessage(), e , ByteUtils.bytesToHex(bytes) );
			return false;
		}

	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public byte[] getBitmap() {
		return bitmap;
	}

	public String getMessageType() {
		return messageType;
	}

	public Map<Object, Object> getDataElements() {
		return dataElements;
	}

	public boolean isParseSuccess() {
		return parseSuccess;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApplicationMessage [messageType=");
		builder.append(messageType);
		builder.append(", bitmap=");
		builder.append(Arrays.toString(bitmap));
		builder.append(", dataElements=");
		builder.append(dataElements);
		builder.append(", bytes=");
		builder.append(ByteUtils.bytesToHex(bytes));
		builder.append("]");
		return builder.toString();
	}

}
